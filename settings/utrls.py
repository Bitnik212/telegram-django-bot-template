__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 05:30"

from django.urls import re_path, include


urlpatterns = [
    re_path('', include(('bot.urls', 'bot'), namespace='bot')),
]
