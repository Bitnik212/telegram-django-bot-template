__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 05:35"

import os

import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings.settings')
django.setup()

from django.conf import settings

from telegram_core.routing.RouterCallbackMessageCommandHandler import RouterCallbackMessageCommandHandler
from telegram_core.TelegramDjangoBot import TelegramDjangoBot

from telegram.ext import (
    Updater
)


def add_handlers(updater):
    dp = updater.dispatcher
    dp.add_handler(RouterCallbackMessageCommandHandler())


def main():
    updater = Updater(bot=TelegramDjangoBot(settings.TELEGRAM_TOKEN), workers=8)
    add_handlers(updater)
    updater.start_polling()
    updater.idle()


if __name__ == "__main__":
    main()
