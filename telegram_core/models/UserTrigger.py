__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:19"

from django.db import models
from django.conf import settings

from telegram_core.models.TelegramAbstractActiveModel import TelegramAbstractActiveModel
from telegram_core.models.Trigger import Trigger


class UserTrigger(TelegramAbstractActiveModel):
    trigger = models.ForeignKey(Trigger, on_delete=models.PROTECT)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT)

    is_sent = models.BooleanField(default=False)
