__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:18"

import json

from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils import timezone

from telegram_core.models.AbstractActiveModel import AbstractActiveModel
from telegram_core.models.BotMenuElem import BotMenuElem


class Trigger(AbstractActiveModel):

    name = models.CharField(max_length=512, unique=True)
    condition_db = models.TextField(help_text='''
    {
        seeds: [1, 2, 3, 4, 5],
        'amount': [{
            'gte': 5,
            'type__contains': 'dd',  // type__in, type
            'duration': '7d'
        }]
    }
    ''')

    min_duration = models.DurationField(
        help_text=_('the minimum period in which there can be 1 notification for a user of this type')
    )
    priority = models.IntegerField(default=1, help_text=_('the more topics will be executed first'))

    botmenuelem = models.ForeignKey(
        BotMenuElem, on_delete=models.PROTECT,
        help_text=_('which trigger message to show')
    )

    @property
    def condition(self):
        if not hasattr(self, '_condition'):
            self._condition = json.loads(self.condition_db)

        return self._condition

    @staticmethod
    def get_timedelta(delta_string:str):
        days = 0
        hours = 0
        for part in delta_string.split():
            if 'd' in part:
                days = float(part.replace('d', ''))
            elif 'h' in part:
                hours = float(part.replace('h', ''))
            else:
                raise ValueError(f'unknown format {part}')

        return timezone.timedelta(days=days, hours=hours)

    def __str__(self):
        return f"T({self.id}, {self.name})"
