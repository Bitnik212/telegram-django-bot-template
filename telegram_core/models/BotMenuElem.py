__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:16"

import json

from django.db import models
from django.conf import settings
from django.utils.translation import gettext_lazy as _

from telegram_core.models.BotMenuElemAttrText import BotMenuElemAttrText
from telegram_core.models.MessageFormatEnum import MessageFormatEnum

from telegram import InlineKeyboardButton  # no lazy text so standard possible to use


class BotMenuElem(models.Model):
    """

    """

    command = models.TextField(  # for multichoice start
        null=True, blank=True,  # todo: add manual check
        help_text=_('Bot command that can call this menu block. Add 1 command per row')
    )

    empty_block = models.BooleanField(
        default=False,
        help_text=_('This block will be shown if there is no catching callback')
    )
    is_visable = models.BooleanField(
        default=True,
        help_text=_('Whether to display this menu block to users (can be hidden and not deleted for convenience)')
    )

    callbacks_db = models.TextField(
        default='[]',
        help_text=_(
            'List of regular expressions (so far only an explicit list) for callbacks that call this menu block. '
            'For example, list ["data", "callback2"] will catch the clicking InlineKeyboardButtons with callback_data "data" or "callback2"'
        )
    )

    forward_message_id = models.IntegerField(null=True, blank=True)
    forward_chat_id = models.IntegerField(null=True, blank=True)

    message_format = models.CharField(max_length=2, choices=MessageFormatEnum.MESSAGE_FORMATS, default=MessageFormatEnum.TEXT)
    message = models.TextField(help_text=_('Text message'))
    buttons_db = models.TextField(
        default='[]',
        help_text=_(
            'InlineKeyboardMarkup buttons structure (double list of dict), where each button(dict) has next format: '
            '{"text": "text", "url": "google.com"} or {"text":"text", "callback_data": "data"})'
        )
    )
    media = models.FileField(help_text=_('File attachment to the message'), null=True, blank=True)
    telegram_file_code = models.CharField(
        max_length=512, null=True, blank=True,
        help_text=_('File code in telegram (must be deleted when replacing file)')
    )

    def __str__(self):
        return f"BME({self.id}, {self.command[:32] if self.command else self.message[:32]})"

    def save(self, *args, **kwargs):
        # bot = telegram.Bot(TELEGRAM_TOKEN)

        super(BotMenuElem, self).save(*args, **kwargs)

        # check and create new models for translation
        if settings.USE_I18N and len(settings.LANGUAGES):
            language_codes = set(map(lambda x: x[0], settings.LANGUAGES))
            if settings.LANGUAGE_CODE in language_codes:
                language_codes.remove(settings.LANGUAGE_CODE)

            get_existed_language_codes = lambda text: set(BotMenuElemAttrText.objects.filter(
                language_code__in=language_codes,
                bot_menu_elem_id=self.id,
                default_text=text,
            ).values_list('language_code', flat=True))

            BotMenuElemAttrText.objects.bulk_create([
                BotMenuElemAttrText(language_code=language_code, default_text=self.message, bot_menu_elem_id=self.id)
                for language_code in language_codes - get_existed_language_codes(self.message)
            ])

            for row_elem in self.buttons:
                for elem in row_elem:
                    if text := elem.get('text'):
                        BotMenuElemAttrText.objects.bulk_create([
                            BotMenuElemAttrText(language_code=language_code, default_text=text,
                                                bot_menu_elem_id=self.id)
                            for language_code in language_codes - get_existed_language_codes(text)
                        ])

    @property
    def buttons(self):
        if not hasattr(self, '_buttons'):
            self._buttons = json.loads(self.buttons_db)

        return self._buttons

    @property
    def callbacks(self):
        if not hasattr(self, '_callbacks'):
            self._callbacks = json.loads(self.callbacks_db)

        return self._callbacks

    def get_message(self, language='en'):
        get_translate_model = None
        if language != settings.LANGUAGE_CODE:
            get_translate_model = BotMenuElemAttrText.objects.filter(
                language_code=language,
                bot_menu_elem_id=self.id,
                default_text=self.message,
                translated_text__isnull=False,
            ).first()
        text = get_translate_model.translated_text if get_translate_model else self.message
        return text

    def get_buttons(self, language='en'):
        # todo: rewrite with 1 request to db (default_text__in=...)

        get_translate_model = lambda text: BotMenuElemAttrText.objects.filter(
            language_code=language,
            bot_menu_elem_id=self.id,
            default_text=text,
            translated_text__isnull=False,
        ).first()

        need_translation = language != settings.LANGUAGE_CODE and settings.USE_I18N
        buttons = []

        for row_elem in self.buttons:
            row_buttons = []
            for item_in_row in row_elem:
                elem = dict(item_in_row)
                if elem.get('text') and need_translation and (translate_model := get_translate_model(elem['text'])):
                    elem['text'] = translate_model.translated_text
                row_buttons.append(InlineKeyboardButton(**elem))

            buttons.append(row_buttons)
        return buttons
