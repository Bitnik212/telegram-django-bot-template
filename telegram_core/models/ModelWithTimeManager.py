__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:10"

from django.db import models


class ModelWithTimeManager(models.Manager):
    def bot_filter_active(self, *args, **kwargs):
        return self.filter(dttm_deleted__isnull=True, *args, **kwargs)
