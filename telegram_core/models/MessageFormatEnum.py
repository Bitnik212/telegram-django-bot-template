__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:07"

from django.utils.translation import gettext_lazy as _


class MessageFormatEnum:
    TEXT = 'T'
    PHOTO = 'P'
    DOCUMENT = 'D'
    AUDIO = 'A'
    VIDEO = 'V'
    GIF = 'G'
    VOICE = 'TV'
    VIDEO_NOTE = 'VN'
    STICKER = 'S'
    LOCATION = 'L'
    GROUP_MEDIA = 'GM'

    MESSAGE_FORMATS = (
        (TEXT, _('Text')),
        (PHOTO, _('Image')),
        (DOCUMENT, _('Document')),
        (AUDIO, _('Audio')),
        (VIDEO, _('Video')),
        (GIF, _('GIF/animation')),
        (VOICE, _('Voice')),
        (VIDEO_NOTE, _('Video note')),
        (STICKER, _('Sticker')),
        (LOCATION, _('Location')),
        (GROUP_MEDIA, _('Media Group')),
    )

    ALL_FORMATS = (elem[0] for elem in MESSAGE_FORMATS)
