__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:11"

from django.db import models

from telegram_core.models.AbstractActiveModel import AbstractActiveModel


class TelegramAbstractActiveModel(AbstractActiveModel):
    message_id = models.BigIntegerField(null=True, blank=True)

    class Meta:
        abstract = True

