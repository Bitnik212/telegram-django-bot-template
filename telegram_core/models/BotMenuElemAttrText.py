__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:17"

from django.db import models
from django.utils import timezone

from django.utils.translation import gettext_lazy as _


class BotMenuElemAttrText(models.Model):
    class Meta:
        unique_together = [['bot_menu_elem', 'language_code', 'default_text']]
        index_together = [['bot_menu_elem', 'language_code', 'default_text']]

    dttm_added = models.DateTimeField(default=timezone.now)
    bot_menu_elem = models.ForeignKey("BotMenuElem", null=False, on_delete=models.CASCADE)

    language_code = models.CharField(max_length=16)
    default_text = models.TextField(
        help_text=_('The text which should be translate')
    )
    translated_text = models.TextField(null=True, help_text=_('Default_text Translation'))

