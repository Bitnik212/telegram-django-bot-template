__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:14"

from django.db import models
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from django.core import validators


class TeleDeepLink(models.Model):
    title = models.CharField(max_length=64, default='', blank=True)
    price = models.FloatField(null=True, blank=True)
    link = models.CharField(max_length=64, validators=[validators.RegexValidator(
        '^[a-zA-Z0-9_-]+$',
        _('Telegram is only accepted letters, numbers and signs - _'),
    )])
    users = models.ManyToManyField(settings.AUTH_USER_MODEL)

    def __str__(self):
        return f'TDL({self.id}, {self.link})'
