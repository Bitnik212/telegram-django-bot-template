__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:06"

import json
import datetime


class TelegramDjangoJsonDecoder(json.JSONDecoder):
    def __init__(self, *args, **kwargs):
        super(TelegramDjangoJsonDecoder, self).__init__(*args, object_hook=self.object_hook_decoder, **kwargs)

    def object_hook_decoder(self, sub_dict, *args, **kwargs):
        # so it works only for dictionary data (if datetime is in list it will not be worked)
        for key in sub_dict.keys():
            value = sub_dict[key]
            dt_object = None
            if type(value) == str:
                try:
                    dt_object = datetime.time.fromisoformat(value)
                except ValueError:
                    try:
                        dt_object = datetime.date.fromisoformat(value)
                    except ValueError:
                        try:
                            dt_object = datetime.datetime.fromisoformat(value)
                        except ValueError:
                            pass

                if dt_object:
                    sub_dict[key] = dt_object

        return sub_dict
