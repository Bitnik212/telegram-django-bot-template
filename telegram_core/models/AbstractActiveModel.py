__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:11"

from django.db import models
from django.utils import timezone
from telegram_core.models.ModelWithTimeManager import ModelWithTimeManager


class AbstractActiveModel(models.Model):
    dttm_added = models.DateTimeField(default=timezone.now)
    dttm_deleted = models.DateTimeField(null=True, blank=True)

    objects = ModelWithTimeManager()

    class Meta:
        abstract = True
