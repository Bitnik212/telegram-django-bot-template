__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:15"

from django.db import models
from django.conf import settings


class ActionLog(models.Model):
    """
    User actions logs
    """

    dttm = models.DateTimeField(auto_now_add=True, db_index=True)
    type = models.CharField(max_length=64)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return 'AL({}, {}, {})'.format(self.user_id, self.dttm, self.type)

