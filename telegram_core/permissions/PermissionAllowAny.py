__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:32"

from telegram_core.permissions.BasePermissionClass import BasePermissionClass


class PermissionAllowAny(BasePermissionClass):
    def has_permissions(self, bot, update, user, utrl_args, **kwargs):
        return True

