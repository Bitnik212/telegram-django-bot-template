__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 22:58"

from telegram_core.admin.CustomModelAdmin import CustomModelAdmin
from django.apps import apps

from telegram_core.admin.utils.CustomRelatedOnlyDropdownFilter import CustomRelatedOnlyDropdownFilter


class TelegramUserAdmin(CustomModelAdmin):
    def __init__(self, model, admin_site) -> None:
        if apps.is_installed('rangefilter'):
            from rangefilter.filters import DateRangeFilter
            self.list_filter = (
                'is_active',
                ('date_joined', DateRangeFilter),
                ('teledeeplink', CustomRelatedOnlyDropdownFilter),
            )
        super().__init__(model, admin_site)

    list_display = ('id', 'first_name', 'last_name', 'telegram_username')
    search_fields = (
        'first_name__startswith',
        'last_name__startswith',
        'username__startswith',
        'id'
    )
    list_filter = (
        'is_active',
        'date_joined',
        ('teledeeplink', CustomRelatedOnlyDropdownFilter),
    )

