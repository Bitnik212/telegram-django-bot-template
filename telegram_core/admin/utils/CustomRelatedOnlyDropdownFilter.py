__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:03"

from django_admin_listfilter_dropdown.filters import RelatedOnlyDropdownFilter


class CustomRelatedOnlyDropdownFilter(RelatedOnlyDropdownFilter):
    template = 'dropdown_filter1.html'
    ordering_field = 'id'

    def field_choices(self, field, request, model_admin):
        pk_qs = model_admin.get_queryset(request).distinct().values_list('%s__pk' % self.field_path, flat=True)
        return field.get_choices(include_blank=False, limit_choices_to={'pk__in': pk_qs}, ordering=[self.ordering_field,])
