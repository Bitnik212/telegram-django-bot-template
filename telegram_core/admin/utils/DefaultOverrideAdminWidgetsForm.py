__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:03"

from django_json_widget.widgets import JSONEditorWidget
from django import forms


class DefaultOverrideAdminWidgetsForm(forms.ModelForm):
    json_fields = []
    list_json_fields = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.json_fields:
            self.fields[field].widget = JSONEditorWidget()

        for field in self.list_json_fields:
            self.fields[field].widget = JSONEditorWidget(attrs={"value": "[]"})

