__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:01"

from django.contrib import admin

from telegram_core.admin.BotMenuElemAdminForm import BotMenuElemAdminForm
from telegram_core.admin.CustomModelAdmin import CustomModelAdmin
from telegram_core.models import BotMenuElem


@admin.register(BotMenuElem)
class BotMenuElemAdmin(CustomModelAdmin):
    list_display = ('id', 'message', 'is_visable', 'callbacks_db')
    search_fields = ('command', 'callbacks_db', 'message', 'buttons_db',)
    list_filter = ('is_visable', 'empty_block')
    form = BotMenuElemAdminForm
