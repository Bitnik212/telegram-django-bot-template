__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:02"

from telegram_core.admin.CustomModelAdmin import CustomModelAdmin
from telegram_core.models import BotMenuElemAttrText
from django.contrib import admin


@admin.register(BotMenuElemAttrText)
class BotMenuElemAttrTextAdmin(CustomModelAdmin):
    list_display = ('id', 'dttm_added', 'language_code', 'default_text', 'translated_text')
    search_fields = ('default_text', 'translated_text')
    list_filter = ('language_code', 'bot_menu_elem')

