__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:01"

from telegram_core.admin.utils.DefaultOverrideAdminWidgetsForm import DefaultOverrideAdminWidgetsForm


class BotMenuElemAdminForm(DefaultOverrideAdminWidgetsForm):
    list_json_fields = ['buttons_db', 'callbacks_db', ]
