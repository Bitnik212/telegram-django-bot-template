__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:05"

from django.contrib import admin

from telegram_core.admin.CustomModelAdmin import CustomModelAdmin
from telegram_core.models import UserTrigger


@admin.register(UserTrigger)
class UserTriggerAdmin(CustomModelAdmin):
    list_display = ('id', 'dttm_added', 'trigger_id', 'user_id', 'is_sent')
    list_filter = ('trigger', 'is_sent')
