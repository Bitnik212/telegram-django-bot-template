__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:05"

from django.contrib import admin

from telegram_core.admin.CustomModelAdmin import CustomModelAdmin
from telegram_core.admin.TriggerAdminForm import TriggerAdminForm
from telegram_core.models import Trigger


@admin.register(Trigger)
class TriggerAdmin(CustomModelAdmin):
    list_display = ('id', 'name', 'min_duration', 'priority', 'botmenuelem_id')
    search_fields = ('name', 'condition_db')
    form = TriggerAdminForm

