__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:00"

from django.contrib import admin

from telegram_core.admin.CustomModelAdmin import CustomModelAdmin
from telegram_core.admin.utils.CustomRelatedOnlyDropdownFilter import CustomRelatedOnlyDropdownFilter
from telegram_core.models.ActionLog import ActionLog


@admin.register(ActionLog)
class ActionLogAdmin(CustomModelAdmin):
    def __init__(self, model, admin_site) -> None:
        if apps.is_installed('rangefilter'):
            from rangefilter.filters import DateRangeFilter
            self.list_filter = (
                'type',
                ('dttm', DateRangeFilter),
                ('user', CustomRelatedOnlyDropdownFilter),
            )
        super().__init__(model, admin_site)

    list_display = ('id', 'user', 'dttm', 'type')
    search_fields = ('type__startswith',)
    list_filter = (
        'type',
        'dttm',
        ('user', CustomRelatedOnlyDropdownFilter),
    )
    raw_id_fields = ('user',)
