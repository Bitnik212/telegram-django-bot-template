__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 22:57"

import csv
from django.utils.translation import gettext_lazy as _
from django.contrib import admin
from django.http import HttpResponse


class CustomModelAdmin(admin.ModelAdmin):
    def export_as_csv(self, request, queryset):

        meta = self.model._meta
        file_name = str(meta).replace('.', '_')

        field_names = [field.name for field in meta.fields]

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(file_name)
        writer = csv.writer(response)

        writer.writerow(field_names)
        for obj in queryset:
            row = writer.writerow([getattr(obj, field) for field in field_names])

        return response

    export_as_csv.short_description = _("Export Selected")
    actions = ('export_as_csv',)
