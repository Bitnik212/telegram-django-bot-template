__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:00"

from django.contrib import admin
from django.db.models import Count

from telegram_core.admin.CustomModelAdmin import CustomModelAdmin
from telegram_core.models import TeleDeepLink


@admin.register(TeleDeepLink)
class TeleDeepLinkAdmin(CustomModelAdmin):
    list_display = ('id', 'title', 'price', 'link', 'count_users')
    search_fields = ('title', 'link')

    def get_queryset(self, request):
        qs = super(TeleDeepLinkAdmin, self).get_queryset(request)
        return qs.annotate(
            c_users=Count('users')
        )

    def count_users(self, inst):
        return inst.c_users

    count_users.admin_order_field = 'c_users'

    def count_activated(self, inst):
        return inst.ca_users

    count_activated.admin_order_field = 'ca_users'

