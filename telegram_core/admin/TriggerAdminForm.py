__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:02"

from telegram_core.admin.utils.DefaultOverrideAdminWidgetsForm import DefaultOverrideAdminWidgetsForm


class TriggerAdminForm(DefaultOverrideAdminWidgetsForm):
    json_fields = ['condition_db',]

