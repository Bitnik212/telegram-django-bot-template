from django.apps import AppConfig


class TelegramDjangoBotConfig(AppConfig):
    name = 'telegram_core'
