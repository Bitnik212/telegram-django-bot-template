__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:29"


class TelegramViewSetMetaClass(type):
    """
    Needed for inheritance information of command_routings_<func> and meta_texts_dict

    """

    def __new__(mcs, name, bases, attrs):
        # Collect fields from current class.

        current_fields = []
        for key, value in list(attrs.items()):
            if key.startswith('command_routing_') and (type(value) == str) and value:
                current_fields.append((key, value))
                attrs.pop(key)
        attrs['command_routings'] = dict(current_fields)
        new_class = super().__new__(mcs, name, bases, attrs)

        # Walk through the MRO.
        command_routings = {}
        show_texts_dict = {}
        for base in reversed(new_class.__mro__):
            # Collect command_routings from base class.
            if hasattr(base, 'command_routings'):
                command_routings.update(base.command_routings)

            if hasattr(base, 'meta_texts_dict'):
                show_texts_dict.update(base.meta_texts_dict)

        new_class.command_routings = command_routings
        new_class.show_texts_dict = show_texts_dict
        return new_class

