__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:43"

from dateutil.relativedelta import relativedelta
from calendar import monthcalendar

from telegram_core.telegram_lib_redefinition import (
    InlineKeyboardButtonDJ as inlinebutt
)


class CalendarPagination:
    def __init__(
            self,
            callback_prefix,
            curr_month,

            buttons: dict = None,
            selected_values=None,
            month_callback_prefix=None,
            month_callback_str_format=None,

            not_clickable=True,
    ):
        self.SELECTED_TICK = '✅ '
        self.PREV_PAGE_STR = '⏮'
        self.NEXT_PAGE_STR = '⏭'

        self.callback_prefix = callback_prefix
        self.curr_month = curr_month
        self.buttons = buttons or {}
        self.selected_values = selected_values or []
        self.month_callback_prefix = month_callback_prefix or callback_prefix
        self.month_callback_str_format = month_callback_str_format or '%y.%m'

        self.not_clickable = not_clickable

    def construct_inline_curr_page(self):
        prev_month = self.curr_month - relativedelta(months=1)
        next_month = self.curr_month + relativedelta(months=1)
        curr_month_callback = self.month_callback_prefix + self.curr_month.strftime(self.month_callback_str_format)

        month_buttons = [
            [
                inlinebutt(
                    self.PREV_PAGE_STR,
                    callback_data=self.month_callback_prefix + prev_month.strftime(self.month_callback_str_format)
                ),
                inlinebutt(
                    self.NEXT_PAGE_STR,
                    callback_data=self.month_callback_prefix + next_month.strftime(self.month_callback_str_format)
                ),
            ]
        ]

        for week_row in monthcalendar(self.curr_month.year, self.curr_month.month):
            week_buttons = []
            for month_day in week_row:
                if month_day > 0:
                    day_button_info = self.buttons.get(month_day)
                    if day_button_info:
                        button_callback, button_text = day_button_info
                    else:
                        button_callback = curr_month_callback if self.not_clickable else self.callback_prefix + f'{month_day}'
                        button_text = f'{month_day}'

                    if month_day in self.selected_values:
                        button_text = f'{self.SELECTED_TICK} {button_text}'
                else:
                    button_text = '\u200b'
                    button_callback = curr_month_callback

                week_buttons.append(
                    inlinebutt(button_text, callback_data=button_callback)
                )
            month_buttons.append(week_buttons)
        return month_buttons

