__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:44"

from telegram_core.telegram_lib_redefinition import (
    InlineKeyboardButtonDJ as inlinebutt
)


class ButtonPagination:
    # todo: rewrite code
    # ButtonPagination WITHOUT WARRANTY
    """
    construct several pages with buttons

    buttons -- array of buttons with values for display to user, button format:
        [text; value]
    selected_buttons -- selected buttons (add icon)
    header_buttons -- buttons in the header for navigation or other cases, format:
       [text; value; callback_prefix]  -- if callback_prefix=None then self.callback_prefix is selected
    footer_buttons -- same as header_buttons, but in the footer

    """

    def __init__(
            self,
            callback_prefix,
            buttons=None,
            selected_values=None,
            callback_prefix_context_values=None,
            rows=8,
            columns=1,
    ):
        self.SELECTED_TICK = '✅ '
        self.PREV_PAGE_STR = '⏮'
        self.NEXT_PAGE_STR = '⏭'
        self.PAGE_CALLBACK_SYMBOL = 'telegram_p'

        self.callback_prefix = callback_prefix
        self.buttons = buttons
        self.callback_prefix_context_values = callback_prefix_context_values
        self.selected_values = selected_values
        self.rows = rows
        self.columns = columns

    @property
    def buttons_per_page(self):
        return self.rows * self.columns

    @property
    def full_callback_prefix(self):
        context_callback = ''
        if self.callback_prefix_context_values:
            context_callback = '-'.join(map(str, self.callback_prefix_context_values)) + '-'
        return self.callback_prefix + context_callback

    def value_page(self, value):
        """
        select the default page for display

        :param value: ???
        :return:
        """

        selected_item_index = list(
            map(lambda x: x[1], self.buttons)
        ).index(value)
        return selected_item_index // self.buttons_per_page

    def _select_page_buttons(self, page_num):
        """
        select buttons for display on the page_num page. Func is created for easy logic redefinition.
        :param page_num: if None, then  _select_page is called
        :return:
        """
        return self.buttons[page_num * self.buttons_per_page: (page_num + 1) * self.buttons_per_page]

    def construct_inline_curr_page(self, page_num=None, ):
        """
        Created  inline buttons

        :param page_num:
        :return:
        """

        telegram_buttons = []
        if page_num is None:
            if self.selected_values:
                page_num = self.value_page(self.selected_values[0])
            else:
                page_num = 0

        value_buttons = self._select_page_buttons(page_num)

        col_index = 0
        for button in value_buttons:
            button_text = ''
            if self.selected_values and (button[1] in self.selected_values):
                button_text += self.SELECTED_TICK

            button_text += button[0]
            button_telegram = inlinebutt(button_text, callback_data=self.full_callback_prefix + button[1])

            if col_index == 0:
                # new row
                telegram_buttons.append([button_telegram])
            else:
                # add in last row
                telegram_buttons[-1].append(button_telegram)

            col_index += 1
            if col_index == self.columns:
                col_index = 0

        # neighbor pages
        neighbor_buttons = []
        if page_num > 0:
            callback_data = self.full_callback_prefix + self.PAGE_CALLBACK_SYMBOL + str(page_num - 1)
            neighbor_buttons.append(
                inlinebutt(self.PREV_PAGE_STR, callback_data=callback_data)
            )
        if page_num < int(len(self.buttons) / self.buttons_per_page + 0.9999) - 1:
            callback_data = self.full_callback_prefix + self.PAGE_CALLBACK_SYMBOL + str(page_num + 1)
            neighbor_buttons.append(
                inlinebutt(self.NEXT_PAGE_STR, callback_data=callback_data)
            )
        if neighbor_buttons:
            telegram_buttons.append(neighbor_buttons)

        return telegram_buttons

