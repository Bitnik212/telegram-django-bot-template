__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:58"

from django.urls import resolve, Resolver404, reverse
from django.conf import settings

from telegram_core.models.BotMenuElem import BotMenuElem
from telegram_core.utils import handler_decor


def telegram_resolve(path, utrl_conf=None):
    if path[0] != '/':
        path = f'/{path}'

    if '?' in path:
        path = path.split('?')[0]

    if utrl_conf is None:
        utrl_conf = settings.TELEGRAM_ROOT_UTRLCONF

    try:
        resolver_match = resolve(path, utrl_conf)
    except Resolver404:
        resolver_match = None
    return resolver_match


def telegram_reverse(viewname, utrl_conf=None, args=None, kwargs=None, current_app=None):
    if utrl_conf is None:
        utrl_conf = settings.TELEGRAM_ROOT_UTRLCONF

    response = reverse(viewname, utrl_conf, args, kwargs, current_app)
    if response[0] == '/':
        response = response[1:]
    return response


@handler_decor(log_type='C')
def all_command_bme_handler(bot, update, user):

    if len(update.message.text[1:]) and 'start' == update.message.text[1:].split()[0]:
        menu_elem = None
        if len(update.message.text[1:]) > 6:  # 'start ' + something
            menu_elem = BotMenuElem.objects.filter(
                command__contains=update.message.text[1:],
                is_visable=True,
            ).first()

        if menu_elem is None:
            menu_elem = BotMenuElem.objects.filter(
                command='start',
                is_visable=True,
            ).first()
    else:
        menu_elem = BotMenuElem.objects.filter(
            command=update.message.text[1:],
            is_visable=True,
        ).first()
    return bot.send_botmenuelem(update, user, menu_elem)


@handler_decor(log_type='C')
def all_callback_bme_handler(bot, update, user):
    menu_elem = BotMenuElem.objects.filter(
        callbacks_db__contains=update.callback_query.data,
        is_visable=True,
    ).first()
    return bot.send_botmenuelem(update, user, menu_elem)
