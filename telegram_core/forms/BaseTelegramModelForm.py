__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:25"

from django.forms.models import BaseModelForm, ModelMultipleChoiceField
from django.db import models
from telegram_core.forms.BaseTelegramForm import BaseTelegramForm


class BaseTelegramModelForm(BaseTelegramForm, BaseModelForm):
    def __init__(self, user, data=None, files=None, initial=None, instance=None):
        self.user = user
        if instance is None:
            data = self._init_helper_get_data(user, data)
        else:
            for model_field in self.base_fields:
                use_from_db = False
                if hasattr(instance, model_field):
                    if self.base_fields[model_field].__class__ == ModelMultipleChoiceField:
                        info_from_user = data.get(model_field)
                        if not info_from_user is None:
                            get_from_user_pks = set(info_from_user)
                            get_from_db = getattr(instance, model_field).all()
                            get_from_db_pks = set(el.pk for el in get_from_db)

                            data[model_field] = self._multichoice_intersection(get_from_user_pks, get_from_db_pks)
                        else:
                            use_from_db = True

                    elif not model_field in data:
                        use_from_db = True

                    if use_from_db:
                        data[model_field] = getattr(instance, model_field)
                        if issubclass(type(data[model_field]), models.Manager):
                            data[model_field] = data[model_field].all()
                else:
                    raise ValueError(
                        f'fields in TelegramModelForm should have same name: {model_field}, {instance.__dict__.keys()}')

        BaseModelForm.__init__(self, data, files, initial=initial, instance=instance)
        # self.error_class = TelegramErrorList

        self.next_field = None
        self.fields, self.next_field = self._init_helper_fields_detection(data)

    def save(self, commit=True, is_completed=True):
        """

        :param commit: save to db (in user on in model)
        :param is_completed: special signal -- if not yet completed and no instance, then save only to user
        :return:
        """

        # there was add condition " len(set(self.base_fields.keys()) - set(self.fields.keys())) == 0: " --
        # but with hidden unnecessary field could be len(set(self.base_fields.keys()) - set(self.fields.keys())) > 0
        if self.is_valid() and self.next_field is None and (is_completed or self.instance.pk):
            # full valid form
            BaseModelForm.save(self, commit=commit)
            self.user.clear_status()
        else:
            BaseTelegramForm.save(self, commit=commit)

