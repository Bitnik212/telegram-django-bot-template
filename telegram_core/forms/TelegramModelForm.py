__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:27"

from django.forms.models import ModelFormMetaclass

from telegram_core.forms.BaseTelegramModelForm import BaseTelegramModelForm


class TelegramModelForm(BaseTelegramModelForm, metaclass=ModelFormMetaclass):
    """just for executing metaclass"""

