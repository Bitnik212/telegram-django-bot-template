__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:23"

from django.forms.forms import ErrorDict


class TelegramErrorDict(ErrorDict):
    def __str__(self):
        return self.as_text()
