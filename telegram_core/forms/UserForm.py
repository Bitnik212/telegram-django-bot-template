__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:38"

from django.utils.translation import gettext_lazy as _
from django.contrib.auth import get_user_model
from django.conf import settings
from django.utils import translation
from django.forms import BaseModelForm

from telegram_core.forms.BaseTelegramForm import BaseTelegramForm
from telegram_core.forms.TelegramModelForm import TelegramModelForm


class UserForm(TelegramModelForm):
    form_name = _('User')

    class Meta:
        model = get_user_model()

        fields = ['timezone', 'telegram_language_code', ]

        labels = {
            "timezone": _("Timezone"),
            'telegram_language_code': _("Language"),
        }

    def save(self, commit=True, is_completed=True):
        if self.is_valid() and self.next_field is None and (is_completed or self.instance.pk):
            # full valid form

            BaseModelForm.save(self, commit=commit)
            self.user.refresh_from_db()
            self.user.clear_status()

            if settings.USE_I18N:
                translation.activate(self.user.language_code)
        else:
            BaseTelegramForm.save(self, commit=commit)
