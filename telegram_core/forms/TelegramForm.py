__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "30.07.2023 23:26"

from django.forms.forms import DeclarativeFieldsMetaclass

from telegram_core.forms.BaseTelegramForm import BaseTelegramForm


class TelegramForm(BaseTelegramForm, metaclass=DeclarativeFieldsMetaclass):
    """just for executing metaclass"""
