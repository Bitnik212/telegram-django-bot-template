"""
    @author Николай Витальевич Никоноров (Bitnik212)
    @date 25.07.2023 23:56
"""


from django.urls import re_path
from telegram import Update

from bot.models import User
from telegram_core.TelegramDjangoBot import TelegramDjangoBot
from telegram_core.utils import handler_decor


@handler_decor()
def start(bot: TelegramDjangoBot, update: Update, user: User):
    print("Ура!!!")
    print(update.message, user)


urlpatterns = [
    re_path('start', start, name='start'),
    re_path('main_menu', start, name='start'),
]
